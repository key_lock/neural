package cn.ms.mconf.ui.service;

import cn.ms.neural.limiter.RuleData;

public interface LimiterService {

	RuleData search(String keywords);

}
