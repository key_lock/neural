package cn.ms.neural.extension.prototype;

import cn.ms.neural.extension.NSPI;

@NSPI
public interface NspiPrototype {
    long spiHello();
}
